/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package managed;

import Entity.Cliente;
import Entity.Producto;
import Sesions.ClienteFacadeLocal;
import Sesions.ProductoFacadeLocal;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author MDCA
 */
@ManagedBean
@SessionScoped
public class JSFManagedBean implements Serializable {

    /**
     * Instancia extraida de uno de los EJB
     */
    @EJB
    private ClienteFacadeLocal managedCliente;
    private List<Cliente> listaClientes;
    private Cliente cliente;
    @EJB
    private ProductoFacadeLocal managedProducto;
    private List<Producto> listaProductos;
    private Producto producto;
    
    public JSFManagedBean() {
    }

    @PostConstruct
    private void inicio() {
        cliente = new Cliente();
        listarClientes();
        producto = new Producto ();
        producto.setValUnitario(0.0);
    }
    
    public void grabarCliente() {
        managedCliente.create(cliente);
        listarClientes();
        cliente = new Cliente();
    }
    
    public void grabarProducto() {
        managedProducto.create(producto);
        listarProductos();
        producto = new Producto();
        producto.setValUnitario(0.0);
    }
    
    public void listarClientes() {
        setListaClientes(managedCliente.findAll());
    }
    
    public void listarProductos() {
        setListaProductos(managedProducto.findAll());
    }
    
    public ClienteFacadeLocal getManagedCliente() {
        return managedCliente;
    }

    public void setManagedCliente(ClienteFacadeLocal managedCliente) {
        this.managedCliente = managedCliente;
    }

    public List<Cliente> getListaClientes() {
        return listaClientes;
    }

    public void setListaClientes(List<Cliente> listaClientes) {
        this.listaClientes = listaClientes;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 13 * hash + Objects.hashCode(this.managedCliente);
        hash = 13 * hash + Objects.hashCode(this.listaClientes);
        hash = 13 * hash + Objects.hashCode(this.cliente);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final JSFManagedBean other = (JSFManagedBean) obj;
        if (!Objects.equals(this.managedCliente, other.managedCliente)) {
            return false;
        }
        if (!Objects.equals(this.listaClientes, other.listaClientes)) {
            return false;
        }
        if (!Objects.equals(this.cliente, other.cliente)) {
            return false;
        }
        return true;
    }

    public ProductoFacadeLocal getManagedProducto() {
        return managedProducto;
    }

    public void setManagedProducto(ProductoFacadeLocal managedProducto) {
        this.managedProducto = managedProducto;
    }

    public List<Producto> getListaProductos() {
        return listaProductos;
    }

    public void setListaProductos(List<Producto> listaProductos) {
        this.listaProductos = listaProductos;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }
    
    
}
